// Fill out your copyright notice in the Description page of Project Settings.

#include "AuthorizationManagerComponent.h"
#include "AuthResponse.h"
#include "TNConstants.h"

// Sets default values for this component's properties
UAuthorizationManagerComponent::UAuthorizationManagerComponent()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = true;
    
    // ...
}


// Called when the game starts
void UAuthorizationManagerComponent::BeginPlay()
{
    Super::BeginPlay();
    
    // ...
    Http = &FHttpModule::Get();
}


// Called every frame
void UAuthorizationManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    
    // ...
}

void UAuthorizationManagerComponent::authenticate(FString url, FAuthConfig config) {
    UE_LOG(LogTemp, Warning, TEXT("In authenticate : %s"), *url);
    FString ContentJsonString = config.getJsonString();
    
    TSharedRef<IHttpRequest> Request = PostRequest(url, ContentJsonString);
    Request->OnProcessRequestComplete().BindUObject(this, &UAuthorizationManagerComponent::authenticateResponse);
    Send(Request);
}

void UAuthorizationManagerComponent::authenticateResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    
    FAuthResponse authResponse;
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        notifyResponseToDelegate(authResponse);
        return;
    }
    
    authResponse.loadFromJsonString(Response->GetContentAsString());
    
    notifyResponseToDelegate(authResponse);
    UE_LOG(LogTemp, Warning, TEXT("SessionId is: %s"), *authResponse.ueSessionId);
}

TSharedRef<IHttpRequest> UAuthorizationManagerComponent::RequestWithRoute(FString Subroute) {
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->SetURL(authBaseUrl + Subroute);
    SetRequestHeaders(Request);
    return Request;
}

void UAuthorizationManagerComponent::notifyResponseToDelegate(FAuthResponse resp) {
    if (delegate != nullptr) {
        delegate->OnReceivedResponse(resp);
    }
}

void UAuthorizationManagerComponent::SetRequestHeaders(TSharedRef<IHttpRequest>& Request) {
    //    Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
    //    Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
    //    Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

//TSharedRef<IHttpRequest> UAuthorizationManagerComponent::GetRequest(FString Subroute) {
//    TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
//    Request->SetVerb("GET");
//    return Request;
//}

TSharedRef<IHttpRequest> UAuthorizationManagerComponent::PostRequest(FString url, FString ContentJsonString) {
    UE_LOG(LogTemp, Warning, TEXT("ContentJSONString: %s"), *ContentJsonString);
    TSharedRef<IHttpRequest> Request = RequestWithRoute(url);
    Request->SetVerb("POST");
    Request->SetHeader("Authorization", "Bearer " + appKey);
    Request->SetHeader("Content-Type", "application/json");
    Request->SetContentAsString(ContentJsonString);
    //    SetAuthorizationValue(appKey, Request);
    return Request;
}

void UAuthorizationManagerComponent::Send(TSharedRef<IHttpRequest>& Request) {
    Request->ProcessRequest();
}

void UAuthorizationManagerComponent::setDelegate(AuthorizationManagerDelegate* del) {
    delegate = del;
}

bool UAuthorizationManagerComponent::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful) {
    UE_LOG(LogTemp, Warning, TEXT("Response Message: %s"), *Response->GetContentAsString());
    if (!bWasSuccessful || !Response.IsValid()) {
        UE_LOG(LogTemp, Warning, TEXT("Inside if"));
        return false;
    }
    if (EHttpResponseCodes::IsOk(Response->GetResponseCode())) {
        UE_LOG(LogTemp, Warning, TEXT("Inside second if"));
        return true;
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("Http Response returned error code: %d"), Response->GetResponseCode());
        return false;
    }
}

void UAuthorizationManagerComponent::SetAuthorizationValue(FString authValue, TSharedRef<IHttpRequest>& Request) {
    Request->SetHeader(authHeaderKey, authValue);
}

//template <typename StructType>
//void UAuthorizationManagerComponent::GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput) {
//    FJsonObjectConverter::UStructToJsonObjectString(StructType::StaticStruct(), &FilledStruct, StringOutput, 0, 0);
//}
//
//template <typename StructType>
//void UAuthorizationManagerComponent::GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput) {
//    StructType StructData;
//    FString JsonString = Response->GetContentAsString();
//    FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
//}

