// Fill out your copyright notice in the Description page of Project Settings.

#include "WebsocketHandler.h"

WebsocketHandler::WebsocketHandler()
{
}

WebsocketHandler::~WebsocketHandler()
{
}

void WebsocketHandler::connect(FString url) {
    UE_LOG(LogTemp, Log, TEXT("In WebsocketHandler connection method"));
    UE_LOG(LogTemp, Log, TEXT("url = %s"), *url);
    module = FModuleManager::LoadModuleChecked<FWebSocketsModule>("WebSockets");
    UE_LOG(LogTemp, Log, TEXT("After creating module"));
    websocket = module.CreateWebSocket(url);
    UE_LOG(LogTemp, Log, TEXT("After creating websocket"));
    websocket->OnConnected().AddSP(this, &WebsocketHandler::HandleWebSocketConnected);
    websocket->OnConnectionError().AddSP(this, &WebsocketHandler::HandleWebSocketConnectionError);
    websocket->OnClosed().AddSP(this, &WebsocketHandler::HandleWebSocketConnectionClosed);
    websocket->OnMessage().AddSP(this, &WebsocketHandler::HandleWebSocketData);
    websocket->Connect();
}

void WebsocketHandler::HandleWebSocketConnected() {
    UE_LOG(LogTemp, Log, TEXT("In New websocket connected method"));
}

void WebsocketHandler::HandleWebSocketConnectionError(const FString& Error) {
    UE_LOG(LogTemp, Log, TEXT("In New websocket connection error %s"), *Error);
}

void WebsocketHandler::HandleWebSocketConnectionClosed(int32 Status, const FString& Reason, bool bWasClean) {
    UE_LOG(LogTemp, Log, TEXT("In New websocket connection closed with Reason = %s"), *Reason);
}

void WebsocketHandler::HandleWebSocketData(const FString& msg) {
    UE_LOG(LogTemp, Log, TEXT("In New websocket msg received - %s"), *msg);
}
