// Fill out your copyright notice in the Description page of Project Settings.

#include "TruNewsUtils.h"

TruNewsUtils::TruNewsUtils()
{
    
}

TruNewsUtils::~TruNewsUtils()
{
}

void TruNewsUtils::logHandler(const char *text) {
    UE_LOG(LogTemp, Log, TEXT("%s"), *FString(UTF8_TO_TCHAR(text)));
}

FString TruNewsUtils::getStringFromJson(TSharedPtr<FJsonObject> jsonObj) {
    FString OutputString;
    TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
    FJsonSerializer::Serialize(jsonObj.ToSharedRef(), Writer);
    return OutputString;
}
