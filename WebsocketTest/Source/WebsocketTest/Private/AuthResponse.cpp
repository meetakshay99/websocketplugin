
#include "AuthResponse.h"
#include "TruNewsUtils.h"
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"

FString FAuthResponse::getJsonString() {
    TSharedPtr<FJsonObject> jsonObj = MakeShareable(new FJsonObject());
    jsonObj->SetNumberField("messageType",(int) messageType);
    jsonObj->SetStringField("errorMessage", errorMessage);
    jsonObj->SetNumberField("errorCode", (int) errorCode);
    jsonObj->SetStringField("status", status);
    jsonObj->SetStringField("ueSessionId", ueSessionId);
    return TruNewsUtils::getStringFromJson(jsonObj);
}

void FAuthResponse::loadFromJsonString(FString jsonString) {
    TSharedPtr<FJsonObject> jsonObject;
    
    //Create a reader pointer to read the json data
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(jsonString);
    
    //Deserialize the json data given Reader and the actual object to deserialize
    if (FJsonSerializer::Deserialize(reader, jsonObject))
    {
        //Get the value of the json object by field name
        messageType = (MessageType) jsonObject->GetIntegerField("messageType");
        errorMessage = jsonObject->GetStringField("errorMessage");
        errorCode = jsonObject->GetIntegerField("errorCode");
        status = jsonObject->GetStringField("status");
        ueSessionId = jsonObject->GetStringField("ueSessionId");
    }
}
