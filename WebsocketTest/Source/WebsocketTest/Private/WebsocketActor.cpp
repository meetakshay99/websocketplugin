// Fill out your copyright notice in the Description page of Project Settings.

#include "WebsocketActor.h"
#include "TNConstants.h"
#include "TruNewsUtils.h"
#include "TruPollyLogger.h"
#include <Sound/SoundWaveProcedural.h>
#include "Components/AudioComponent.h"
#include <aws/text-to-speech/TextToSpeechManager.h>

Aws::TextToSpeech::CapabilityInfo m_selectedCaps;

// Sets default values
AWebsocketActor::AWebsocketActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWebsocketActor::BeginPlay()
{
	Super::BeginPlay();
    PerformPolly();
	ConnectToServer();
}

void AWebsocketActor::PerformPolly() {
    
    TruPollyLogger::setLogHandler(AWebsocketActor::logHandler);
    
    pollyPtr = MakeShared<TruPolly>();
    audioComponent = FindComponentByClass<UAudioComponent>();
    if (audioComponent != nullptr) {
        audioComponent->OnAudioFinished.AddDynamic(this, &AWebsocketActor::handleAudioFinish);
    }

    char sampleRate[256];
    snprintf(sampleRate, sizeof sampleRate, "%zu", m_selectedCaps.sampleRate);
    UE_LOG(LogTemp, Log, TEXT("sample Rate = %s"), sampleRate);
    pollyPtr->setSampleRate(sampleRate);
    pollyPtr->setOutputFormat(Aws::Polly::Model::OutputFormat::pcm);
    
    UE_LOG(LogTemp, Log, TEXT("Before making a call to getPollyData"));
    pollyPtr->getPollyData("This is sample test of polly plugin in TruNews app", false, [this](const char* text, const std::vector<unsigned char> audioData, const char* error) mutable {
        UE_LOG(LogTemp, Log, TEXT("Got Polly Response in actor"));
        if (strcmp(error,"") != 0) {
            logError("Received error while performing Polly request", error);
        } else {
            const uint8 *reqdAudioData = audioData.data();
            //        const unsigned char *audioDataChar = reinterpret_cast<unsigned char*>(const_cast<unsigned char*>(audioData.data()));
            //
            //        const uint8 *reqdAudioData = static_cast<const uint8*>(audioDataChar);
            
            USoundWaveProcedural *audio = NewObject<USoundWaveProcedural>();
            audio->SetSampleRate((int32)m_selectedCaps.sampleRate); //(22050);
            audio->NumChannels = 1;
            audio->Duration = INDEFINITELY_LOOPING_DURATION;
            audio->SoundGroup = SOUNDGROUP_Voice;
            audio->bLooping = false;
            audio->QueueAudio(reqdAudioData, audioData.size());
            
            AsyncTask(ENamedThreads::GameThread, [audio, this]() {
                // code to execute on game thread here
                
                // Get the audio component and set audio to be played.
                this->audioComponent->SetSound((USoundBase*)audio);
                
                // Activate.
                this->audioComponent->Play();
                
                UE_LOG(LogTemp, Log, TEXT("Played audio via component"));
                
            });
        }
    });
    UE_LOG(LogTemp, Log, TEXT("After making a call to getPollyData"));
}

void AWebsocketActor::handleAudioFinish() {
    UE_LOG(LogTemp, Log, TEXT("In ACharacterSpawner::handleAudioFinish"));
    //    PerformPolly();
}

void AWebsocketActor::logError(const char *msg, const char *error) {
    UE_LOG(LogTemp, Log, TEXT("%s : %s"), *FString(UTF8_TO_TCHAR(msg)), *FString(UTF8_TO_TCHAR(error)));
}

void AWebsocketActor::logHandler(const char *text) {
    UE_LOG(LogTemp, Log, TEXT("%s"), *FString(UTF8_TO_TCHAR(text)));
}

// Called every frame
void AWebsocketActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWebsocketActor::ConnectToServer() {
    UAuthorizationManagerComponent *authComp = Cast<UAuthorizationManagerComponent>(this->GetComponentByClass(UAuthorizationManagerComponent::StaticClass()));
    if (authComp != nullptr) {
        FAuthConfig authConfig;
        authConfig.messageType = MessageType::UE_HTTP_AUTH;
        FDateTime now = FDateTime::Now();
        authConfig.ueId = now.GetTicks();
        authConfig.ueName = "Akshay";
        authConfig.opalPublicIp = "192.167.0.1";
        authConfig.dockerPrivateIp = "192.167.0.1";
        UE_LOG(LogTemp, Log, TEXT("Before calling authenticate method."));
        authComp->setDelegate(this);
        authComp->authenticate(authSubUrl, authConfig);
        UE_LOG(LogTemp, Log, TEXT("After calling authenticate method."));
    } else {
        UE_LOG(LogTemp, Log, TEXT("Authorization Component not found."));
    }
}

void AWebsocketActor::OnReceivedResponse(FAuthResponse authResp) {
    if (authResp.errorCode == 0) {
        UE_LOG(LogTemp, Log, TEXT("Succesfully authenticated. Will now connect to websocket."));
        TSharedPtr<FJsonObject> params = MakeShareable(new FJsonObject);
        
        params->SetStringField("ueSessionId", authResp.ueSessionId);
        params->SetNumberField("messageType", (int32) MessageType::UE_SOCKET_CONNECT);
        FString UriParams = "?jsonRequest=" + FBase64::Encode(TruNewsUtils::getStringFromJson(params));
        FString finalUri = (websocketBaseUrl + websocketSubUrl + UriParams);
        UE_LOG(LogTemp, Log, TEXT("Final websocket Url = %s"), *finalUri);
        
        wsHandler = MakeShareable(new WebsocketHandler);
        wsHandler->connect(finalUri);
    } else {
        UE_LOG(LogTemp, Log, TEXT("Failed to authenticate. So not connecting over websocket. Error => %s"), *authResp.errorMessage);
    }
}
