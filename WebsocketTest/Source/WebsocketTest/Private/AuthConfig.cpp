
#include "AuthConfig.h"
#include "TruNewsUtils.h"
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"

FString FAuthConfig::getJsonString() {
    TSharedPtr<FJsonObject> jsonObj = MakeShareable(new FJsonObject());
    jsonObj->SetNumberField("messageType",(int) messageType);
    jsonObj->SetNumberField("ueId", ueId);
    jsonObj->SetStringField("ueName", ueName);
    jsonObj->SetStringField("opalPublicIp", opalPublicIp);
    jsonObj->SetStringField("dockerPrivateIp", dockerPrivateIp);
    return TruNewsUtils::getStringFromJson(jsonObj);
}

void FAuthConfig::loadFromJsonString(FString jsonString) {
    TSharedPtr<FJsonObject> jsonObject;
    
    //Create a reader pointer to read the json data
    TSharedRef<TJsonReader<>> reader = TJsonReaderFactory<>::Create(jsonString);
    
    //Deserialize the json data given Reader and the actual object to deserialize
    if (FJsonSerializer::Deserialize(reader, jsonObject))
    {
        //Get the value of the json object by field name
        messageType = (MessageType) jsonObject->GetIntegerField("messageType");
        ueId = jsonObject->GetIntegerField("ueId");
        ueName = jsonObject->GetStringField("ueName");
        opalPublicIp = jsonObject->GetStringField("opalPublicIp");
        dockerPrivateIp = jsonObject->GetStringField("dockerPrivateIp");
    }
}

