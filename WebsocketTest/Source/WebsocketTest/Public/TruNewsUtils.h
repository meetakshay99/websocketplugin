// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 *
 */
class WEBSOCKETTEST_API TruNewsUtils
{
public:
    TruNewsUtils();
    ~TruNewsUtils();
    
    static void logHandler(const char *text);
    static FString getStringFromJson(TSharedPtr<FJsonObject> jsonObj);
};
