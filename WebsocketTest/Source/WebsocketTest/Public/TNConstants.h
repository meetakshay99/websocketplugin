// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

extern const FString authBaseUrl;
extern const FString authSubUrl;
extern const FString websocketBaseUrl;
extern const FString websocketSubUrl;
extern const FString appKey;
extern const FString authHeaderKey;
