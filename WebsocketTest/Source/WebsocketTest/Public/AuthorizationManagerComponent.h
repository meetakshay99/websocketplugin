// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TNEnums.h"
#include "AuthConfig.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "AuthResponse.h"
#include "AuthorizationManagerComponent.generated.h"

class AuthorizationManagerDelegate {
public:
    virtual void OnReceivedResponse(FAuthResponse authResp) = 0;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WEBSOCKETTEST_API UAuthorizationManagerComponent : public UActorComponent
{
    GENERATED_BODY()
    
private:
    AuthorizationManagerDelegate *delegate;
    FHttpModule* Http;
    
    TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
    void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);
    
    TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
    void Send(TSharedRef<IHttpRequest>& Request);
    
    bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);
    void SetAuthorizationValue(FString authValue, TSharedRef<IHttpRequest>& Request);
    
    template <typename StructType>
    void GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput);
    template <typename StructType>
    void GetStructFromJsonString(FHttpResponsePtr Response, StructType& StructOutput);
    
    void notifyResponseToDelegate(FAuthResponse resp);
    
public:
    // Sets default values for this component's properties
    UAuthorizationManagerComponent();
    
    void setDelegate(AuthorizationManagerDelegate* delegate);
    void authenticate(FString url, FAuthConfig config);
    void authenticateResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
protected:
    // Called when the game starts
    virtual void BeginPlay() override;
    
public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
    
    
    
};
