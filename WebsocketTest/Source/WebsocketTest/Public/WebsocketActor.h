// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AuthorizationManagerComponent.h"
#include "AuthResponse.h"
#include "GameFramework/Actor.h"
#include "TruPolly.h"
#include "WebsocketHandler.h"
#include "WebsocketActor.generated.h"

UCLASS()
class WEBSOCKETTEST_API AWebsocketActor : public AActor, public AuthorizationManagerDelegate
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWebsocketActor();

protected:
    TSharedPtr<TruPolly> pollyPtr;
    UAudioComponent *audioComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    void PerformPolly();
    
    UFUNCTION()
    void handleAudioFinish();
    void logError(const char *msg, const char *error);
    static void logHandler(const char *text);
    
    TSharedPtr<WebsocketHandler> wsHandler;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    void ConnectToServer();
    void OnReceivedResponse(FAuthResponse authResp);
	
	
};
