// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM()
enum class MessageType : uint8 {
    UE_HTTP_AUTH = 0,
    UE_SOCKET_CONNECT = 1,
    UE_SOCKET_HEART_BEAT = 2,
    UE_SOCKET_MESSAGE = 3,
    UE_SOCKET_CLOSE = 4,
    UE_SOCKET_ERROR = 5,
    UE_SERVER_MESSAGE = 6,
    UE_OPAL_LOOKUP = 7
};
