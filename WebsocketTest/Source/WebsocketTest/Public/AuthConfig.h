// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TNEnums.h"
#include "AuthConfig.generated.h"

USTRUCT()
struct FAuthConfig {
    GENERATED_BODY()
public:
    MessageType messageType;
    int64 ueId;
    FString ueName;
    FString opalPublicIp;
    FString dockerPrivateIp;
    
    FString getJsonString();
    void loadFromJsonString(FString jsonString);
};

