// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IWebSocket.h"
#include "WebSocketsModule.h"

/**
 * 
 */
class WEBSOCKETTEST_API WebsocketHandler : public TSharedFromThis<WebsocketHandler>
{
protected:
    TSharedPtr<IWebSocket> websocket;
    FWebSocketsModule module;
    
    
    void HandleWebSocketConnected();
    void HandleWebSocketConnectionError(const FString& Error);
    void HandleWebSocketConnectionClosed(int32 Status, const FString& Reason, bool bWasClean);
    void HandleWebSocketData(const FString& msg);
    
public:
    void connect(FString url);
    WebsocketHandler();
    ~WebsocketHandler();};
