// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TNEnums.h"
#include "AuthResponse.generated.h"

UENUM()
enum class ResponseType : uint8 {
    OK,
    FAIL
};

USTRUCT()
struct FAuthResponse {
    GENERATED_BODY()
public:
    MessageType messageType;
    FString errorMessage;
    int errorCode;
    FString status;
    FString ueSessionId;
    
    FString getJsonString();
    void loadFromJsonString(FString jsonString);
};


