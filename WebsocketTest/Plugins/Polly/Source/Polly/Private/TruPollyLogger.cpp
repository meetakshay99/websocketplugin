// Fill out your copyright notice in the Description page of Project Settings.

#include "TruPollyLogger.h"
#include <string>

using namespace std;

static LogHandler logHandler = nullptr;

void TruPollyLogger::setLogHandler(LogHandler handler) {
    logHandler = handler;
}

void TruPollyLogger::log(string text) {
    if (logHandler != nullptr) {
        logHandler(text.c_str());
    }
}
