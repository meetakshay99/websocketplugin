// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;
using System.Diagnostics;

//For Tools.DotNETCommon.JsonObject and Tools.DotNETCommon.FileReference
using Tools.DotNETCommon;

public class Polly : ModuleRules
{
    private string ThirdPartyPath {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../ThirdParty/")); }
    }
    
    public bool LoadPolly(ReadOnlyTargetRules Target) {
        Log.TraceInformation("LoadPolly 1");
        
        // Start Polly linking here!
        bool isLibrarySupported = false;
        Log.TraceInformation("LoadPolly 2");
        
        // Create Polly Path.
        string PollyPath = Path.Combine(ThirdPartyPath, "AWSPolly");
        Log.TraceInformation("PollyPath = " + PollyPath);
        Log.TraceInformation("LoadPolly 3");
        
        // Get Library Path
        String LibPath = "";
        String HeaderPath = "";
        if (Target.Platform == UnrealTargetPlatform.Mac) {
            LibPath = Path.Combine(PollyPath, "Libraries", "Mac");
            HeaderPath = Path.Combine(PollyPath, "Includes");
            isLibrarySupported = true;
            Log.TraceInformation("LoadPolly 4 Mac");
            
        } else if (Target.Platform == UnrealTargetPlatform.Linux) {
            //            LibPath = Path.Combine(PollyPath, "Libraries", "Linux");
            //            HeaderPath = Path.Combine(PollyPath, "Includes");
            //            isLibrarySupported = true;
            //            Log.TraceInformation("LoadPolly 4 Linux");
        } else {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
            Log.TraceInformation("LoadPolly 5");
        }
        
        Log.TraceInformation("LoadPolly 6");
        if (isLibrarySupported) {
            Log.TraceInformation("LibPath = " + LibPath);
            Log.TraceInformation("HeaderPath = " + HeaderPath);
            
            PublicLibraryPaths.AddRange(new string[] { LibPath });
            PublicIncludePaths.AddRange(new string[] { HeaderPath });
            
            DirectoryInfo libPathDirInfo = new DirectoryInfo(LibPath);
            FileInfo[] files = libPathDirInfo.GetFiles();
            foreach (FileInfo file in files) {
                string tempPath = Path.Combine(LibPath, file.Name);
                PublicAdditionalLibraries.Add(tempPath);
            }
        }
        Log.TraceInformation("LoadPolly 7");
        return isLibrarySupported;
    }
    
    private string ModuleRoot {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../..")); }
    }
    
    private bool IsWindows(ReadOnlyTargetRules target) {
        return (target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64);
    }
    
    private void ProcessDependencies(string depsJson, ReadOnlyTargetRules target)
    {
        //We need to ensure libraries end with ".lib" under Windows
        string libSuffix = ((this.IsWindows(target)) ? ".lib" : "");
        
        //Attempt to parse the JSON file
        JsonObject deps = JsonObject.Read(new FileReference(depsJson));
        
        //Process the list of dependencies
        foreach (JsonObject dep in deps.GetObjectArrayField("dependencies"))
        {
            //Add the header and library paths for the dependency package
            PublicIncludePaths.AddRange(dep.GetStringArrayField("include_paths"));
            PublicLibraryPaths.AddRange(dep.GetStringArrayField("lib_paths"));
            
            //Add the preprocessor definitions from the dependency package
            PublicDefinitions.AddRange(dep.GetStringArrayField("defines"));
            
            //Link against the libraries from the package
            string[] libs = dep.GetStringArrayField("libs");
            foreach (string lib in libs)
            {
                string libFull = lib + ((libSuffix.Length == 0 || lib.EndsWith(libSuffix)) ? "" : libSuffix);
                PublicAdditionalLibraries.Add(libFull);
            }
        }
    }
    
    public Polly(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        
        PublicIncludePaths.AddRange(new string[] { "Polly/Public", "ThirdParty/AWSPolly/Includes" });
        PublicDependencyModuleNames.AddRange(new string[] {"Core", "Projects"});
        PrivateIncludePaths.AddRange(new string[] { });
        DynamicallyLoadedModuleNames.AddRange(new string[] { });
            Log.TraceInformation("In Polly Plugin");
        
        if (Target.Platform == UnrealTargetPlatform.Mac) {
            PrivateIncludePaths.AddRange(new string[] { "Polly/Private" });
            PrivateDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine" });
            LoadPolly(Target);
        } else if (Target.Platform == UnrealTargetPlatform.Linux) {
            Log.TraceInformation("Will now compile polly plugin for linux");
            PrivateIncludePaths.AddRange(new string[] { "Polly/Private" });
            PrivateDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine" });
            
            //Install third-party dependencies using conan
            Process.Start(new ProcessStartInfo
            {
                FileName = "conan",
                Arguments = "install . --profile ue4",
                WorkingDirectory = ModuleRoot
            })
            .WaitForExit();
            
            //Link against our conan-installed dependencies
            this.ProcessDependencies(Path.Combine(ModuleRoot, "conanbuildinfo.json"), Target);
        } else {
            PrivateIncludePaths.AddRange(new string[] { "Polly/Private", "ThirdParty/AWSPolly/Includes" });
        }
    }
}
