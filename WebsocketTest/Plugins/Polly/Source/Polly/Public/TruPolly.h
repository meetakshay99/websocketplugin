// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <stdio.h>
#include <vector>
#include <aws/polly/model/OutputFormat.h>

class TruPollyInternal;

typedef std::function<void(const char* text, const std::vector<unsigned char> audioData, const char* error)> PollyDataHandler;

class POLLY_API TruPolly {
    
private:
    TruPollyInternal* ppInternal;
    //    PollyProviderInternal *ppInternal;
    void notifyIfPossible(const char *text, std::vector<unsigned char> audioDataVal, const char* error, PollyDataHandler handler);
    
public:
    TruPolly();
    void testMethod();
    void getPollyData(const char *text, bool isSSML, PollyDataHandler handler);
    void setOutputFormat(Aws::Polly::Model::OutputFormat format);
    void setSampleRate(const char *sampleRate);
};
