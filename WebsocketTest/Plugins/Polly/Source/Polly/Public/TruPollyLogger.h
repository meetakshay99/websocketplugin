// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include <stdio.h>
#include <functional>

typedef std::function<void(const char* text)> LogHandler;

using namespace std;

class TruPollyLogger {
public:
    static void setLogHandler(LogHandler handler);
    static void log(string msg);
};
