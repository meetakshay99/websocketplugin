// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;
using System.Diagnostics;

//For Tools.DotNETCommon.JsonObject and Tools.DotNETCommon.FileReference
using Tools.DotNETCommon;

public class Websocket : ModuleRules
{
    private string ThirdPartyPath {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../ThirdParty/")); }
    }
    
    public bool LoadWebsocketPlugin(ReadOnlyTargetRules Target) {
        Log.TraceInformation("LoadWebsocketPlugin 1");
        
        // Start Polly linking here!
        bool isLibrarySupported = false;
        Log.TraceInformation("LoadWebsocketPlugin 2");
        
        // Get Library Path
        String LibPath = "";
        String HeaderPath = "";
        if (Target.Platform == UnrealTargetPlatform.Mac) {
            LibPath = Path.Combine(ThirdPartyPath, "lib", "Mac");
            HeaderPath = Path.Combine(ThirdPartyPath, "include", "Mac");
            isLibrarySupported = true;
            Log.TraceInformation("LoadWebsocketPlugin 3 Mac");
            
        } else if (Target.Platform == UnrealTargetPlatform.Linux) {
            //            LibPath = Path.Combine(PollyPath, "Libraries", "Linux");
            //            HeaderPath = Path.Combine(PollyPath, "Includes");
            //            isLibrarySupported = true;
            //            Log.TraceInformation("LoadPolly 4 Linux");
        } else {
            string Err = string.Format("{0} dedicated server is made to depend on {1}. We want to avoid this, please correct module dependencies.", Target.Platform.ToString(), this.ToString()); System.Console.WriteLine(Err);
            Log.TraceInformation("LoadWebsocketPlugin 4");
        }
        
        Log.TraceInformation("LoadWebsocketPlugin 5");
        if (isLibrarySupported) {
            Log.TraceInformation("LibPath = " + LibPath);
            Log.TraceInformation("HeaderPath = " + HeaderPath);
            
            PublicLibraryPaths.AddRange(new string[] { LibPath });
            PublicIncludePaths.AddRange(new string[] { HeaderPath });
            
            DirectoryInfo libPathDirInfo = new DirectoryInfo(LibPath);
            FileInfo[] files = libPathDirInfo.GetFiles();
            foreach (FileInfo file in files) {
                string tempPath = Path.Combine(LibPath, file.Name);
                PublicAdditionalLibraries.Add(tempPath);
            }
        }
        Log.TraceInformation("LoadWebsocketPlugin 6");
        return isLibrarySupported;
    }
    
    private string ModuleRoot {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../..")); }
    }
    
    private bool IsWindows(ReadOnlyTargetRules target) {
        return (target.Platform == UnrealTargetPlatform.Win32 || Target.Platform == UnrealTargetPlatform.Win64);
    }
    
    private void ProcessDependencies(string depsJson, ReadOnlyTargetRules target)
    {
        //We need to ensure libraries end with ".lib" under Windows
        string libSuffix = ((this.IsWindows(target)) ? ".lib" : "");
        
        //Attempt to parse the JSON file
        JsonObject deps = JsonObject.Read(new FileReference(depsJson));
        
        //Process the list of dependencies
        foreach (JsonObject dep in deps.GetObjectArrayField("dependencies"))
        {
            //Add the header and library paths for the dependency package
            PublicIncludePaths.AddRange(dep.GetStringArrayField("include_paths"));
            PublicLibraryPaths.AddRange(dep.GetStringArrayField("lib_paths"));
            
            //Add the preprocessor definitions from the dependency package
            PublicDefinitions.AddRange(dep.GetStringArrayField("defines"));
            
            //Link against the libraries from the package
            string[] libs = dep.GetStringArrayField("libs");
            foreach (string lib in libs)
            {
                string libFull = lib + ((libSuffix.Length == 0 || lib.EndsWith(libSuffix)) ? "" : libSuffix);
                PublicAdditionalLibraries.Add(libFull);
            }
        }
    }
    
	public Websocket(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        
        PublicDependencyModuleNames.AddRange(new string[] {"Core", "Json", "CoreUObject", "JsonUtilities", "Projects"});
        PrivateIncludePaths.AddRange(new string[] { });
        DynamicallyLoadedModuleNames.AddRange(new string[] { });
        
        if (Target.Platform == UnrealTargetPlatform.Mac) {
            Definitions.Add("PLATFORM_UWP=0");
            PublicIncludePaths.AddRange(new string[] { "Websocket/Public", "ThirdParty/include/Mac" });
            PrivateIncludePaths.AddRange(new string[] { "Websocket/Private" });
            PrivateDependencyModuleNames.AddRange(new string[] { "Engine", "CoreUObject" });
            LoadWebsocketPlugin(Target);
        } else if (Target.Platform == UnrealTargetPlatform.Linux) {
            Definitions.Add("PLATFORM_UWP=0");
            PublicIncludePaths.AddRange(new string[] { "Websocket/Public", "ThirdParty/include/Linux" });
            PrivateIncludePaths.AddRange(new string[] { "Websocket/Private" });
            PrivateDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine" });
            
            //Install third-party dependencies using conan
            Process.Start(new ProcessStartInfo
            {
                FileName = "conan",
                Arguments = "install . --profile ue4",
                WorkingDirectory = ModuleRoot
            })
            .WaitForExit();
            
            //Link against our conan-installed dependencies
            this.ProcessDependencies(Path.Combine(ModuleRoot, "conanbuildinfo.json"), Target);
        } else {
            PrivateDependencyModuleNames.AddRange(new string[] { "Engine", "CoreUObject" });
        }
	}
}
